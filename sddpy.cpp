/*
 * This file is part of sddpy.
 *
 * sddpy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sddpy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sddpy.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Python.h>
#include "lib/lib.h"
#include "utils.cpp"
//#include <stdio.h>

typedef struct {
  PyObject_HEAD
  Libs *libs;
} Dicts;

static PyObject *sddpyError;

static void
Dicts_dealloc(Dicts* self)
{
  delete self->libs;
  //printf("bye\n");
}

static int
Dicts_init(Dicts *self, PyObject *args, PyObject *kwds)
{
  
  const char * dir;

  if (!PyArg_ParseTuple(args, "s", &dir))
    return NULL;
  strlist_t dicts_dir_list, empty_list, disable_list;
  dicts_dir_list.push_back(dir);
  self->libs = new Libs();
  //printf("hi,%s\n", dir);
  self->libs->load(dicts_dir_list, empty_list, disable_list);
  //printf("hi\n");
  return 0;
}

static PyObject *
Dicts_ndicts(Dicts* self)
{
  return Py_BuildValue("i", self->libs->ndicts());
}

static PyObject *
Dicts_narticles(Dicts* self, PyObject *args)
{
  int id;
  if (!PyArg_ParseTuple(args, "i", &id))
    return NULL;

  return Py_BuildValue("i", self->libs->narticles(id));
}

static PyObject *
Dicts_get_word(Dicts* self, PyObject *args)
{
  int did; //dictionary id
  glong wid; //word id
  if (!PyArg_ParseTuple(args, "il", &did, &wid))
    return NULL;

  const gchar *word=self->libs->poGetWord(wid, did);

  if (word)
    return Py_BuildValue("s", word);
  else
    return NULL;
}

static PyObject *
Dicts_dict_name(Dicts* self, PyObject *args)
{
  int id;
  if (!PyArg_ParseTuple(args, "i", &id))
    return NULL;

  return Py_BuildValue("s", self->libs->dict_name(id).c_str());
}

static PyObject *
Dicts_lookup(Dicts* self, PyObject *args)
{
  int id;
  const char *word;
  if (!PyArg_ParseTuple(args, "is", &id, &word))
    return NULL;
  glong g;
  if (self->libs->SimpleLookupWord(word, g, id))
    {
      gchar *word_data=self->libs->poGetWordData(g,id);
      return Py_BuildValue("s", parse_data(word_data).c_str());
    }
  else
    {
      PyErr_SetString(sddpyError, "Word not found");
      return NULL;
    }
}

static PyMethodDef Dicts_methods[] = {
  {"ndicts", (PyCFunction)Dicts_ndicts, METH_NOARGS,
   "Return the number of dictionaries"},
  {"get_word", (PyCFunction)Dicts_get_word, METH_VARARGS,
   "Return the word from dictionary by it's index"},
  {"narticles", (PyCFunction)Dicts_narticles, METH_VARARGS,
   "Return the number of articles in dictionary"},
  {"dict_name", (PyCFunction)Dicts_dict_name, METH_VARARGS,
   "Return the name of dictionary"},
  {"lookup", (PyCFunction)Dicts_lookup, METH_VARARGS,
   "Lookup word"},
  {NULL}  /* Sentinel */
};





static PyTypeObject DictsType = {
  PyObject_HEAD_INIT(NULL)
  0,                         /*ob_size*/
  "_sddpy.Dicts",              /*tp_name*/
  sizeof(Dicts),             /*tp_basicsize*/
  0,                         /*tp_itemsize*/
  (destructor)Dicts_dealloc, /*tp_dealloc*/
  0,                         /*tp_print*/
  0,                         /*tp_getattr*/
  0,                         /*tp_setattr*/
  0,                         /*tp_compare*/
  0,                         /*tp_repr*/
  0,                         /*tp_as_number*/
  0,                         /*tp_as_sequence*/
  0,                         /*tp_as_mapping*/
  0,                         /*tp_hash */
  0,                         /*tp_call*/
  0,                         /*tp_str*/
  0,                         /*tp_getattro*/
  0,                         /*tp_setattro*/
  0,                         /*tp_as_buffer*/
  Py_TPFLAGS_DEFAULT,        /*tp_flags*/
  "Dicts objects",           /* tp_doc */
  0,               /* tp_traverse */
  0,               /* tp_clear */
  0,               /* tp_richcompare */
  0,               /* tp_weaklistoffset */
  0,               /* tp_iter */
  0,               /* tp_iternext */
  Dicts_methods,             /* tp_methods */
  0,             /* tp_members */
  0,                         /* tp_getset */
  0,                         /* tp_base */
  0,                         /* tp_dict */
  0,                         /* tp_descr_get */
  0,                         /* tp_descr_set */
  0,                         /* tp_dictoffset */
  (initproc)Dicts_init,      /* tp_init */
  0,                         /* tp_alloc */
  0,                 /* tp_new */
};

static PyMethodDef sddpy_methods[] = {
  {NULL}  /* Sentinel */
};


#ifndef PyMODINIT_FUNC/* declarations for DLL import/export */
#define PyMODINIT_FUNC void
#endif
PyMODINIT_FUNC
init_sddpy(void) 
{
  PyObject* m;

  DictsType.tp_new = PyType_GenericNew;
  if (PyType_Ready(&DictsType) < 0)
    return;
  
  m = Py_InitModule3("_sddpy", sddpy_methods,
		     "Stardict dictionaries module.");

  
  Py_INCREF(&DictsType);

  PyModule_AddObject(m, "Dicts", (PyObject *)&DictsType);

  sddpyError = PyErr_NewException("_sddpy.error", NULL, NULL);
  Py_INCREF(sddpyError);

  PyModule_AddObject(m, "error", sddpyError);

}

