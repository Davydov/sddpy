import sddpy #import sddpy module

# This creates Dicts object. This is usually several dictionaries factory.
# You can get dictionaries from here:
# http://stardict.sourceforge.net/Dictionaries.php
# The default locations for dictionaries are
# /usr/share/stardict/dic (system wide) and $HOME/.stardict/dic.
dicts = sddpy.Dicts('/usr/share/stardict/dic')

# Let's print names of all loaded dictionaries.
for dict in dicts:
    print dict.name

# Now let's try to translate word.
word='test'
for dict in dicts:
    try:
        print 'The translation of "%s" from %s.' % (word, dict.name)
        print dict.lookup(word)
    except sddpy.LookupError: # Word not found.
        print "Not found"

# Now let's access a dictionary by name.
# Suppose you have Mueller7GPL:
# http://stardict.sourceforge.net/Dictionaries_dictd-www.mova.org.php
mueller=dicts['Mueller7GPL']
print mueller.lookup(word)
# This is the same.
print mueller[word]
for word in mueller:
    # This is not effective method for search.
    # You probably could use this to create some kind of external index.
    if word == 'test':
        print 'Found "test".'
        break
