# This file is part of sddpy.

# sddpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# sddpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with sddpy.  If not, see <http://www.gnu.org/licenses/>.

import _sddpy
class Error(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class LookupError(Error):
    pass

class Dict(object):
    def __init__(self, dicts, key):
        self._dicts = dicts
        self.name = key
        self.narticles = self._dicts.narticles(self.name)
 
    def lookup(self, word):
        return self._dicts.lookup(self.name, word.encode('utf-8'))

    def __getitem__(self, key):
        return self.lookup(key)

    def __len__(self):
        return self.narticles

    def __iter__(self):
        self.i = 0
        return self

    def next(self):
        if self.i<self.narticles:
            word=self._dicts.get_word(self.name, self.i)
            self.i+=1
            return word
        else:
            raise StopIteration

class Dicts(object):
    def __init__(self, path):
        self.path = path
        self._dicts = _sddpy.Dicts(path)
        self.ids = {}
        for i in xrange(self._dicts.ndicts()):
            self.ids[self._dicts.dict_name(i)]=i

    def __getitem__(self, key):
        return Dict(self, key)
    
    def __iter__(self):
        self.i=0
        return self

    def next(self):
        if self.i<self._dicts.ndicts():
            name=self._dicts.dict_name(self.i)
            self.i+=1
            return self[name]
        else:
            raise StopIteration

    def lookup(self, dict, word):
        try:
            return self._dicts.lookup(self.ids[dict], word)
        except _sddpy.error:
            raise LookupError('word not found')

    def narticles(self, dict):
        return self._dicts.narticles(self.ids[dict])

    def get_word(self, dict, i):
        word = self._dicts.get_word(self.ids[dict], i)
        if word:
            return word
        else:
            raise IndexError('no such index in dictionary')

if __name__ == '__main__':
    d=Dicts('/home/davidov/.stardict/dic')
    #print d.ids
    #print d['Mueller7GPL'].lookup('word')
    #print d.lookup('Mueller7GPL', 'wordd')
    for j in xrange(2):
        for i in d:
            print i.name
            try:
                print i.lookup(u'word')
            except LookupError:
                print "Not found"
