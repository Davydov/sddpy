from distutils.core import setup, Extension

module1 = Extension('sddpy._sddpy',
                    sources = ['sddpy.cpp',
                               'utils.cpp',
                               'lib/dictziplib.cpp',
                               'lib/distance.cpp',
                               'lib/lib.cpp'],
                    library_dirs = ['/usr/local/lib'],
                    include_dirs = ['/usr/include/glib-2.0', 
                                    '/usr/lib/glib-2.0/include',
                                    '/usr/local/include/glib-2.0',
                                    '/usr/lib/x86_64-linux-gnu/glib-2.0/include/',
                                    '/usr/lib/i386-linux-gnu/glib-2.0/include/'],
                    libraries = ['glib-2.0', 'z'],
                    #extra_compile_args = ['-g'],
                    define_macros = [('HAVE_MMAP', 1)])



setup (name = 'sddpy',
       version = '0.2',
       description = 'Stardict dictionary python module.',
       author = 'Iakov Davydov',
       packages = ['sddpy'],
       ext_modules = [module1])
